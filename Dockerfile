#FROM nginx
#COPY ./dist/spa-mat /usr/share/nginx/html

FROM node:9-slim
ENV PORT 8080
EXPOSE 8080
WORKDIR /usr/src/app
COPY ./dist/spa-mat .
RUN npm install -g http-server
CMD http-server .